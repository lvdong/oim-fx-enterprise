package com.oim.core.business.manage;

import java.util.List;

import com.oim.core.bean.Department;
import com.oim.core.bean.DepartmentMember;
import com.oim.core.business.box.DepartmentBox;
import com.oim.core.business.box.HeadBox;
import com.oim.core.business.box.UserDataBox;
import com.oim.core.common.util.AppCommonUtil;
import com.oim.fx.view.EnterpriseMainView;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractManager;
import com.onlyxiahui.im.bean.UserData;
import com.onlyxiahui.im.bean.UserHead;

/**
 * 描述： 主界面好友列表、群列表的管理
 * 
 * @author XiaHui
 * @date 2015年4月12日 上午10:18:18
 * @version 0.0.1
 */
public class DepartmentManage extends AbstractManager {

	public DepartmentManage(AppContext appContext) {
		super(appContext);
		initEvent();
	}

	private void initEvent() {
	}

	public List<String> getDepartmentMemberIdListIncludeNode(String departmentId) {
		DepartmentBox db = appContext.getBox(DepartmentBox.class);
		return db.getDepartmentMemberIdListIncludeNode(departmentId);
	}

	public Department getDepartment(String id) {
		DepartmentBox db = appContext.getBox(DepartmentBox.class);
		return db.getDepartment(id);
	}

	public List<Department> getDepartmentList() {
		DepartmentBox db = appContext.getBox(DepartmentBox.class);
		return db.getDepartmentList();
	}

	public void setDepartmentWithUserList(List<Department> departmentList, List<UserData> userDataList, List<DepartmentMember> departmentMemberList) {
		// DepartmentBox db = appContext.getBox(DepartmentBox.class);
		// db.setDepartmentList(departmentList);
		// db.setDepartmentMemberList(departmentMemberList);

		DepartmentBox db = appContext.getBox(DepartmentBox.class);
		db.setDepartmentMemberList(departmentMemberList);
		db.setDepartmentList(departmentList);

		setUserDataList(userDataList);
		setDepartmentList(departmentList);
		setDepartmentMemberList(departmentMemberList);
	}

	public void setDepartmentList(List<Department> departmentList) {
		EnterpriseMainView mainView = this.appContext.getSingleView(EnterpriseMainView.class);
		mainView.clearDepartmentList();
		if (null != departmentList) {
			mainView.setDepartmentList(departmentList);
		}
	}

	public void setUserDataList(List<UserData> userDataList) {
		if (null != userDataList) {
			for (UserData userData : userDataList) {
				addOrUpdateUserData(userData);
			}
		}
	}

	public void setDepartmentMemberList(List<DepartmentMember> departmentMemberList) {
		if (null != departmentMemberList) {
			for (DepartmentMember departmentMember : departmentMemberList) {
				addDepartmentMember(departmentMember);
			}
			updateDepartmentMemberCount();
		}
	}

	public void addOrUpdateDepartment(Department department) {
		EnterpriseMainView mainView = this.appContext.getSingleView(EnterpriseMainView.class);
		mainView.addOrUpdateDepartment(department);
	}

	public void addOrUpdateDepartmentUserData(String departmentId, UserData userData) {
		EnterpriseMainView mainView = this.appContext.getSingleView(EnterpriseMainView.class);
		mainView.addOrUpdateDepartmentUserData(departmentId, userData);
	}

	/***
	 * 插入好友分组信息
	 * 
	 * @Author: XiaHui
	 * @Date: 2016年2月16日
	 * @ModifyUser: XiaHui
	 * @ModifyDate: 2016年2月16日
	 * @param departmentMember
	 */
	public void addDepartmentMember(DepartmentMember departmentMember) {
		EnterpriseMainView mainView = this.appContext.getSingleView(EnterpriseMainView.class);
		String userId = departmentMember.getUserId();
		String departmentId = departmentMember.getDepartmentId();

		UserDataBox ub = appContext.getBox(UserDataBox.class);
		UserData userData = ub.getUserData(userId);
		if (null != userData) {
			mainView.addOrUpdateDepartmentUserData(departmentId, userData);
		}
	}

	/**
	 * 插入一个用户到好友分组列表
	 * 
	 * @Author: XiaHui
	 * @Date: 2016年2月16日
	 * @ModifyUser: XiaHui
	 * @ModifyDate: 2016年2月16日
	 * @param userData
	 * @param departmentMember
	 */
	public void add(UserData userData, DepartmentMember departmentMember) {
		addOrUpdateUserData(userData);
		addDepartmentMember(departmentMember);
		updateDepartmentMemberCount(departmentMember.getDepartmentId());
	}

	/**
	 * 更新所有的好友分组的在线人数和总人数
	 * 
	 * @Author: XiaHui
	 * @Date: 2016年2月16日
	 * @ModifyUser: XiaHui
	 * @ModifyDate: 2016年2月16日
	 */
	public void updateDepartmentMemberCount() {
		List<Department> departmentList = getDepartmentList();
		for (Department department : departmentList) {
			updateDepartmentMemberCount(department.getId());
		}
	}

	/**
	 * 更新好友分组的总数量和在线人数
	 * 
	 * @Author: XiaHui
	 * @Date: 2016年2月16日
	 * @ModifyUser: XiaHui
	 * @ModifyDate: 2016年2月16日
	 * @param departmentId
	 */
	public void updateDepartmentMemberCount(String departmentId) {
		List<String> departmentMemberIdList = getDepartmentMemberIdListIncludeNode(departmentId);
		int totalCount = 0;
		int onlineCount = 0;
		for (String userId : departmentMemberIdList) {
			UserDataBox ub = appContext.getBox(UserDataBox.class);
			UserData userData = ub.getUserData(userId);
			if (null != userData) {
				totalCount++;
				if (!AppCommonUtil.isOffline(userData.getStatus())) {
					onlineCount++;
				}
			}
		}
		EnterpriseMainView mainView = this.appContext.getSingleView(EnterpriseMainView.class);
		mainView.updateDepartmentMemberCount(departmentId, totalCount, onlineCount);
	}

	public void addUserHead(UserHead userHead) {
		HeadBox hb = appContext.getBox(HeadBox.class);
		hb.putUserHead(userHead);
	}

	public void addOrUpdateUserData(UserData userData) {
		if (null != userData) {
			UserDataBox ub = appContext.getBox(UserDataBox.class);
			ub.putUserData(userData);
			DepartmentBox db = appContext.getBox(DepartmentBox.class);
			List<DepartmentMember> list = db.getUserInDepartmentMemberList(userData.getId());
			if (null != list) {
				EnterpriseMainView mainView = this.appContext.getSingleView(EnterpriseMainView.class);
				for (DepartmentMember dm : list) {
					String departmentId = dm.getDepartmentId();
					mainView.addOrUpdateDepartmentUserData(departmentId, userData);
				}
			}
		}
	}
}
