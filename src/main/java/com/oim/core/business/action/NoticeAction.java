package com.oim.core.business.action;

import com.oim.core.business.service.NoticeService;
import com.only.general.annotation.action.ActionMapping;
import com.only.general.annotation.action.MethodMapping;
import com.only.general.annotation.parameter.Define;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractAction;
import com.onlyxiahui.im.message.data.notice.TextNotice;

/**
 * 描述：
 * 
 * @author 夏辉
 * @date 2014年6月14日 下午9:31:55
 * @version 0.0.1
 */

@ActionMapping(value = "1.700")
public class NoticeAction extends AbstractAction {

	public NoticeAction(AppContext appContext) {
		super(appContext);
	}

	@MethodMapping(value = "1.2.2001")
	public void textNotice(
			@Define("textNotice") TextNotice textNotice) {
		NoticeService ns = appContext.getService(NoticeService.class);
		ns.showNotice(textNotice);
	}
}
