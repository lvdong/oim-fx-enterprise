package com.oim.core.business.sender;

import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.im.message.Head;
import com.onlyxiahui.im.message.client.Message;

/**
 * 
 * @author XiaHui
 *
 */
public class DepartmentSender extends BaseSender {

	public DepartmentSender(AppContext appContext) {
		super(appContext);
	}

	public void getUserList() {

		Message message = new Message();
		Head head = new Head();
		head.setAction("1.1100");
		head.setMethod("1.1.0001");
		head.setTime(System.currentTimeMillis());
		message.setHead(head);
		this.write(message);
	}

	public void getDepartmentList() {
		Message message = new Message();
		Head head = new Head();
		head.setAction("1.1100");
		head.setMethod("1.1.0002");
		head.setTime(System.currentTimeMillis());
		message.setHead(head);
		this.write(message);
	}

	public void getDepartmentMemberList() {

		Message message = new Message();
		Head head = new Head();
		head.setAction("1.1100");
		head.setMethod("1.1.0003");
		head.setTime(System.currentTimeMillis());
		message.setHead(head);
		this.write(message);
	}
	
	public void getDepartmentWithUserList() {

		Message message = new Message();
		Head head = new Head();
		head.setAction("1.1100");
		head.setMethod("1.1.0004");
		head.setTime(System.currentTimeMillis());
		message.setHead(head);
		this.write(message);
	}


	public void getUserHeadList() {

		Message message = new Message();
		Head head = new Head();
		head.setAction("1.1100");
		head.setMethod("1.1.0005");
		head.setTime(System.currentTimeMillis());
		message.setHead(head);
		this.write(message);
	}

	public void getDepartment(String departmentId) {
		Message message = new Message();
		message.put("departmentId", departmentId);
		
		Head head = new Head();
		head.setAction("1.1100");
		head.setMethod("1.1.0007");
		head.setTime(System.currentTimeMillis());
		message.setHead(head);
		this.write(message);
	}
}
