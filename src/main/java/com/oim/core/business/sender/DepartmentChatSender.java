package com.oim.core.business.sender;

import com.only.net.data.action.DataBackAction;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.im.message.Head;
import com.onlyxiahui.im.message.client.Message;
import com.onlyxiahui.im.message.data.chat.Content;
import com.onlyxiahui.im.message.data.query.ChatQuery;
import com.onlyxiahui.im.message.data.PageData;

/**
 * @author XiaHui
 * @date 2015年3月16日 下午3:23:23
 */
public class DepartmentChatSender extends BaseSender {

	public DepartmentChatSender(AppContext appContext) {
		super(appContext);
	}

	public void sendDepartmentChatMessage(String departmentId, String userId, Content content) {
		Message message = new Message();

		message.put("departmentId", departmentId);//
		message.put("userId", userId);// 发送人的id
		message.put("content", content);// 聊天内容

		Head head = new Head();
		head.setAction("1.500");
		head.setMethod("1.1.5001");
		head.setTime(System.currentTimeMillis());
		message.setHead(head);
		this.write(message);
	}

	public void queryDepartmentChatLog(String departmentId, ChatQuery chatQuery, PageData page, DataBackAction dataBackAction) {
		Message message = new Message();

		message.put("departmentId", departmentId);
		message.put("chatQuery", chatQuery);// 聊天内容
		message.put("page", page);

		Head head = new Head();
		head.setAction("1.500");
		head.setMethod("1.1.5004");
		head.setTime(System.currentTimeMillis());
		message.setHead(head);
		this.write(message, dataBackAction);
	}
}
