package com.oim.core.cover.service;

import com.oim.core.business.manage.DepartmentManage;
import com.oim.core.business.manager.PromptManager;
import com.oim.core.business.manager.UserLastManager;
import com.oim.core.business.manager.UserListManager;
import com.oim.core.business.sender.UserSender;
import com.oim.core.common.sound.SoundHandler;
import com.only.general.annotation.parameter.Define;
import com.only.net.action.Back;
import com.only.net.data.action.DataBackAction;
import com.only.net.data.action.DataBackActionAdapter;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractService;
import com.onlyxiahui.im.bean.UserData;

/**
 * 描述：
 * 
 * @author 夏辉
 * @date 2014年3月31日 上午11:45:15 version 0.0.1
 */
public class EnterpriseUserService extends AbstractService {

	public EnterpriseUserService(AppContext appContext) {
		super(appContext);
	}

	/***
	 * 更新用户信息
	 * 
	 * @Author: XiaHui
	 * @Date: 2016年2月16日
	 * @ModifyUser: XiaHui
	 * @ModifyDate: 2016年2月16日
	 * @param userData
	 */
	public void updateUserData(UserData userData) {
		UserListManager listManage = this.appContext.getManager(UserListManager.class);
		UserLastManager lastManage = this.appContext.getManager(UserLastManager.class);
		DepartmentManage departmentManage = this.appContext.getManager(DepartmentManage.class);
		
		listManage.updateUserData(userData);
		//lastManage.addOrUpdateLastUser(userData);
		departmentManage.addOrUpdateUserData(userData);
		
		departmentManage.updateDepartmentMemberCount();
		
		listManage.updateCategoryMemberCount();
		if (UserData.status_online.equals(userData.getStatus())) {// 如果用户是上线信息，那么播放好友上线提醒声音
			PromptManager pm = this.appContext.getManager(PromptManager.class);
			pm.playSound(SoundHandler.sound_type_status);
		}
	}

	public void updateUserData(String userId) {
		DataBackAction dataBackAction = new DataBackActionAdapter() {

			@Back
			public void back(@Define("userData") UserData userData) {
				if (null != userData) {
					UserListManager listManage = appContext.getManager(UserListManager.class);
					listManage.addUserData(userData);
					updateUserData(userData);
				}
			}
		};
		UserSender uh = this.appContext.getSender(UserSender.class);
		uh.getUserDataById(userId, dataBackAction);
	}

	public void updateUserStatus(String userId, String status) {

	}
}
