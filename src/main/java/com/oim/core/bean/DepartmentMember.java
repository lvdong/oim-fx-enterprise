package com.oim.core.bean;

/**
 * 部门成员
 * 
 * @author XiaHui
 * @date 2017年8月13日 上午8:21:32
 */
public class DepartmentMember {

	private String departmentId;
	private String userId;

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
