package com.oim.core.bean;

import java.util.Date;

/**
 * @author XiaHui
 * @date 2017年8月13日 上午8:21:14
 */
public class Department {

	private String id;
	private String superiorId;// 上级id
	private String name;
	private String remark = "";
	private Date createTime;// 建立时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSuperiorId() {
		return superiorId;
	}

	public void setSuperiorId(String superiorId) {
		this.superiorId = superiorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
