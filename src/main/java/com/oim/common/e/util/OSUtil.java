package com.oim.common.e.util;

import com.only.common.util.OnlySystemUtil;

public class OSUtil {

	public static boolean isUnderOrXP() {

		boolean isClassicWindows = false;
		if (OnlySystemUtil.isWindows()) {
			String osVersion = System.getProperty("os.version");
			if (osVersion != null) {
				Float version = Float.valueOf(osVersion);
				if (version.floatValue() < 6.0) {
					isClassicWindows = true;
				} else {
					isClassicWindows = false;
				}
			}
		}
		return isClassicWindows;
	}

	public static void main(String[] agr) {
		String osVersion = System.getProperty("os.version");
		System.out.println(osVersion);
	}
}
