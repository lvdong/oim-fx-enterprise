package com.oim.fx.view.e;

import com.oim.fx.common.component.BaseStage;
import com.oim.fx.common.component.WaitingPane;
import com.oim.fx.ui.chat.pane.MessageList;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.Callback;

/**
 * 设备列表
 * 
 * @author: XiaHui
 * @date: 2017年4月11日 上午9:02:43
 */
public class PaneChatHistoryFrame extends BaseStage {

	Pagination page = new Pagination();// 分页组件
	StackPane centerPane = new StackPane();
	final MessageList  messageList = new MessageList();
	VBox showBox = new VBox();
	WaitingPane waitingPanel = new WaitingPane();
	ScrollPane scrollPane = new ScrollPane();

	public PaneChatHistoryFrame() {
		initComponent();
		iniEvent();
	}

	private void initComponent() {
		this.setTitle("聊天记录");
		//this.setResizable(false);
		this.setWidth(550);
		this.setHeight(380);
		//this.setTitlePaneStyle(2);
		this.setRadius(5);
		BorderPane rootPane = new BorderPane();
		this.setCenter(rootPane);

		Label titleLabel = new Label();
		titleLabel.setText("聊天记录");
		titleLabel.setFont(Font.font("微软雅黑", 14));
		titleLabel.setStyle("-fx-text-fill:rgba(255, 255, 255, 1)");
		
		HBox topBox = new HBox();
		topBox.setStyle("-fx-background-color:#2cb1e0");
		topBox.setPrefHeight(30);
		topBox.setPadding(new Insets(5, 10, 5, 10));
		topBox.setSpacing(10);
		topBox.getChildren().add(titleLabel);

		showBox.getChildren().add(messageList);
		VBox.setVgrow(messageList, Priority.ALWAYS);

		centerPane.getChildren().add(showBox);
		centerPane.getChildren().add(waitingPanel);

		//centerPane.setStyle("-fx-background-color:rgba(8, 103, 68, 0.5)");
		centerPane.setStyle("-fx-background-color:rgba(255, 255, 255)");
		
		rootPane.setTop(topBox);
		rootPane.setCenter(centerPane);
		rootPane.setBottom(page);

		this.showWaiting(false, WaitingPane.show_waiting);
		this.setPage(0, 1);
	}

	private void iniEvent() {
		// TODO Auto-generated method stub
	}

	public void showWaiting(boolean show, String key) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				waitingPanel.setVisible(show);
				showBox.setVisible(!show);
				waitingPanel.show(key);
			}
		});
	}

	public void setPage(int currentPage, int totalPage) {
		page.setCurrentPageIndex(currentPage);
		page.setPageCount(totalPage);
	}

	public void setTotalPage(int totalPage) {
		page.setPageCount(totalPage);
	}

	public void setPageFactory(Callback<Integer, Node> value) {
		page.setPageFactory(value);
	}

	public MessageList getMessageList() {
		return messageList;
	}
}
