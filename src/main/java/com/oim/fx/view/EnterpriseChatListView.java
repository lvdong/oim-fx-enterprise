package com.oim.fx.view;

import com.oim.core.bean.Department;
import com.onlyxiahui.app.base.view.View;
import com.onlyxiahui.im.bean.UserData;
import com.onlyxiahui.im.message.data.chat.Content;

/**
 * 描述：
 *
 * @author XiaHui
 * @date 2015年3月14日 上午11:14:38
 * @version 0.0.1
 */
public interface EnterpriseChatListView extends View {

	public boolean isDepartmentChatShowing(String departmentId);

	public boolean hasDepartmentChat(String departmentId);

	public void show(Department department);

	public void departmentChat(Department department, UserData userData, Content content);

	public void updateDepartmentUserList(String departmentId);

}
