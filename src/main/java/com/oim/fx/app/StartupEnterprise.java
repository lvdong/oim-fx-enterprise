package com.oim.fx.app;

import com.oim.common.e.util.OSUtil;
import com.oim.core.business.manager.SystemManager;
import com.oim.core.business.module.ActionFactory;
import com.oim.core.business.sender.DepartmentSender;
import com.oim.core.business.view.ChatListView;
import com.oim.core.business.view.LoginView;
import com.oim.core.business.view.MainView;
import com.oim.core.business.view.NoticeView;
import com.oim.core.business.view.UserChatHistoryView;
import com.oim.core.common.action.CallAction;
import com.oim.core.cover.action.EnterpriseUserAction;
import com.oim.e.fx.view.NoticeViewImpl;
import com.oim.fx.view.EnterpriseChatListView;
import com.oim.fx.view.EnterpriseChatListViewImpl;
import com.oim.fx.view.EnterpriseMainView;
import com.oim.fx.view.SimpleLoginViewImpl;
import com.oim.fx.view.SimpleMainViewImpl;

import com.oim.fx.view.e.PaneUserChatHistoryViewImpl;
import com.oim.fx.view.pane.PaneEnterpriseChatListViewImpl;

/**
 * @author XiaHui
 * @date 2017年9月7日 下午8:44:02
 */
public class StartupEnterprise extends Launcher {

	protected void registerView() {
		super.registerView();
		if (OSUtil.isUnderOrXP()) {
			appContext.register(NoticeView.class, NoticeViewImpl.class);
			appContext.register(ChatListView.class, PaneEnterpriseChatListViewImpl.class);
			appContext.register(EnterpriseChatListView.class, PaneEnterpriseChatListViewImpl.class);
			//appContext.register(UserChatHistoryView.class, PaneUserChatHistoryViewImpl.class);
		}else {
			appContext.register(ChatListView.class, EnterpriseChatListViewImpl.class);
			appContext.register(EnterpriseChatListView.class, EnterpriseChatListViewImpl.class);
		}
		appContext.register(LoginView.class, SimpleLoginViewImpl.class);
		appContext.register(MainView.class, SimpleMainViewImpl.class);
		appContext.register(EnterpriseMainView.class, SimpleMainViewImpl.class);
		
		//appContext.getSingleView(EnterpriseChatListView.class);
	}

	protected void initPartView() {
		
		if (OSUtil.isUnderOrXP()) {
			PaneEnterpriseChatListViewImpl chatListViewImpl = new PaneEnterpriseChatListViewImpl(appContext);
			PaneUserChatHistoryViewImpl paneUserChatHistoryViewImpl = new PaneUserChatHistoryViewImpl(appContext);
			
			appContext.put(PaneEnterpriseChatListViewImpl.class, chatListViewImpl);
			appContext.put(PaneUserChatHistoryViewImpl.class, paneUserChatHistoryViewImpl);
		}else {
			EnterpriseChatListViewImpl enterpriseChatListViewImpl = new EnterpriseChatListViewImpl(appContext);
			appContext.put(EnterpriseChatListViewImpl.class, enterpriseChatListViewImpl);
		}
	}


	protected void startThread() {
		super.startThread();
		ActionFactory actionFactory = appContext.getModule(ActionFactory.class);
		actionFactory.cover(EnterpriseUserAction.class);
		SystemManager sm = appContext.getManager(SystemManager.class);
		sm.addInitAction(new CallAction() {

			@Override
			public void execute() {
				DepartmentSender ds = appContext.getSender(DepartmentSender.class);
				ds.getUserHeadList();
				ds.getDepartmentWithUserList();
			}
		});
	}
}
