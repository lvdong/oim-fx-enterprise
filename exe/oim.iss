; 脚本由 Inno Setup 脚本向导 生成！
; 有关创建 Inno Setup 脚本文件的详细资料请查阅帮助文档！

[Setup]
; 注: AppId的值为单独标识该应用程序。
; 不要为其他安装程序使用相同的AppId值。
; (生成新的GUID，点击 工具|在IDE中生成GUID。)
AppId={{C22511E6-B06A-4A7C-A4E4-604A57EFD3EE}
AppName=OIM
AppVersion=1.0.0
;AppVerName=OIM 1.0.0
AppPublisher=onlyxiahui
AppPublisherURL=http://www.oimismine.com/
AppSupportURL=http://www.oimismine.com/
AppUpdatesURL=http://www.oimismine.com/
DefaultDirName={pf}\OIM
DisableProgramGroupPage=yes
OutputDir=C:\Data\Workspaces
OutputBaseFilename=oim-install
SetupIconFile=C:\Data\oim\exe.ico
Compression=lzma
SolidCompression=yes
WizardImageFile=b.bmp

[Languages]
Name: "chinesesimp"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.1

[Files]
Source: "C:\Data\oim\oim-x32.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\only\.oim\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; 注意: 不要在任何共享系统文件上使用“Flags: ignoreversion”

[Icons]
Name: "{commonprograms}\OIM"; Filename: "{app}\oim-x32.exe"
Name: "{commondesktop}\OIM"; Filename: "{app}\oim-x32.exe"; Tasks: desktopicon
Name: "{commondesktop}\OIM"; Filename: "{app}\oim-x32.exe"; WorkingDir: "{app}"; 
[Run]
Filename: "{app}\oim-x32.exe"; Description: "{cm:LaunchProgram,OIM}"; Flags: nowait postinstall skipifsilent

